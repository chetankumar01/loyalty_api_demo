import React from 'react' ;
import { View, Text, FlatList } from 'react-native';
import axios from 'axios';

class RewardsList extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      getLoyaltyStatus: null,
      loyalties: null,
    }
  }
  componentDidMount() {
    console.log(axios);
    this.setState({ getLoyaltyStatus: 'started' });
    const config = {
      method: 'GET',
      url: 'https://loyalty.collectapps.io/api/v1/rewards?PageNumber=&PageSize=&CreatedAfter=&CreatedBefore=',
      headers: {
        'Authorization': 'Apikey hu0BgORHLmFGYbsJpY8vUuSIoa9aBc',
      },
    };
    axios(config).then((response) =>{
      this.setState({
        getLoyaltyStatus: 'success',
        loyalties: response.data.Rewards,
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  renderItem = ({ item }) => (
    <View>
      <View style={{ height: 45 , justifyContent: 'center' , paddingLeft: 10}}>
        <Text>{item.Title}</Text>
      </View>
      <View style={{ flex:1, height: 0.5, backgroundColor: 'grey' }} />
    </View>
  );

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View
            style={{
              height: 60,
              alignItems: 'center',
              justifyContent: 'center',
            }}
        >
          <Text>Rewards List</Text>
        </View>
        {this.state.getLoyaltyStatus === 'success' && (
           <View style={{ flex: 1 }}>
             {this.state.loyalties.length && (
                <FlatList
                    data={this.state.loyalties}
                    renderItem={this.renderItem}
                    keyExtractor={item => item.Id}
                />
              )}
           </View>
         )}
        {this.state.getLoyaltyStatus === 'started' && (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Fetching Loyalties...</Text> 
          </View>
         )}
      </View>
    );
  }
}

export default RewardsList;
