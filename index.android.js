import React from 'react';
import { AppRegistry } from 'react-native';
import RewardsList from './app/RewardsList';

AppRegistry.registerComponent('demo_loyalty_api', () => RewardsList);
